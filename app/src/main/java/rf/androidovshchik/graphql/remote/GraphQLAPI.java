package rf.androidovshchik.graphql.remote;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rf.androidovshchik.graphql.models.QLResponse;

public interface GraphQLAPI {

    String URL = "http://52.29.178.41:8888/api/v1/";

    @Headers("Content-Type: application/json")
    @POST("stations")
    Call<String> getToken(@Body RequestBody body);

    @Headers({
        "Content-Type: application/json",
        "Accept: application/json"
    })
    @POST("stations")
    Call<QLResponse> getQLResponse(@HeaderMap Map<String, String> headers, @Body RequestBody body);
}