package rf.androidovshchik.graphql;

import android.app.Application;

import com.google.gson.GsonBuilder;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rf.androidovshchik.graphql.remote.GraphQLAPI;

public class AppMain extends Application {

    public static GraphQLAPI API;

    public static MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    @Override
    public void onCreate() {
        super.onCreate();
        // init retrofit
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(GraphQLAPI.URL)
            .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                .setLenient()
                .create()))
            .client(httpClient.build())
            .build();
        API = retrofit.create(GraphQLAPI.class);
    }
}
