package rf.androidovshchik.graphql.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vlad on 26.04.18.
 */

public class QLResponse {

    @SerializedName("data")
    public QLData data;

    @Override
    public String toString() {
        return "QLResponse{" +
            "data=" + data +
            '}';
    }
}
