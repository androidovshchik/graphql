package rf.androidovshchik.graphql.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vlad on 26.04.18.
 */

public class QLData {

    @SerializedName("orders")
    public ArrayList<QLOrder> orders;

    @Override
    public String toString() {
        return "QLData{" +
            "orders=" + orders +
            '}';
    }
}
