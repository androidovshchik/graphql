package rf.androidovshchik.graphql.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vlad on 26.04.18.
 */

public class QLFuel {

    @SerializedName("name")
    public String name;

    @SerializedName("type")
    public String type;

    @Override
    public String toString() {
        return "QLFuel{" +
            "name='" + name + '\'' +
            ", type='" + type + '\'' +
            '}';
    }
}
