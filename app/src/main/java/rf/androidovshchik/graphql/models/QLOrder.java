package rf.androidovshchik.graphql.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vlad on 26.04.18.
 */

public class QLOrder {

    @SerializedName("_id")
    public String id;

    @SerializedName("created")
    public Long created;

    @SerializedName("fuel")
    public QLFuel fuel;

    @Override
    public String toString() {
        return "QLOrder{" +
            "id='" + id + '\'' +
            ", created=" + created +
            ", fuel=" + fuel +
            '}';
    }
}
