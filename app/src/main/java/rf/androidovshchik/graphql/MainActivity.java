package rf.androidovshchik.graphql;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rf.androidovshchik.graphql.models.QLResponse;

public class MainActivity extends AppCompatActivity {

    private static String token = null;

    private TextView tokenView;
    private TextView responseView;

    private String login = "gas158";
    private String password = "pass";

    @Override
    @SuppressWarnings("all")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tokenView = findViewById(R.id.token);
        responseView = findViewById(R.id.response);
        tokenView.setText("TOKEN: " + token);
        responseView.setText("RESPONSE: null");
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(token)) {
                    JSONObject object = new JSONObject();
                    try {
                        object.put("login", login);
                        object.put("pass", password);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Invalid json for login", Toast.LENGTH_SHORT)
                            .show();
                        return;
                    }
                    AppMain.API.getToken(RequestBody.create(AppMain.JSON, object.toString()))
                        .enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                token = response.body();
                                tokenView.setText("TOKEN: " + token);
                                if (!TextUtils.isEmpty(token)) {
                                    getQLResponse();
                                } else {
                                    Toast.makeText(getApplicationContext(), "Invalid login response", Toast.LENGTH_SHORT)
                                        .show();
                                }
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {
                                t.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Failed login", Toast.LENGTH_SHORT)
                                    .show();
                            }
                        });
                } else {
                    getQLResponse();
                }
            }
        });
    }

    @SuppressWarnings("all")
    private void getQLResponse() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Cookie", "token=" + token);
        JSONObject object = new JSONObject();
        try {
            object.put("query", "query{ orders{_id created fuel { name type } } }");
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Invalid json for query", Toast.LENGTH_SHORT)
                .show();
            return;
        }
        AppMain.API.getQLResponse(headers, RequestBody.create(AppMain.JSON, object.toString()))
            .enqueue(new Callback<QLResponse>() {
                @Override
                public void onResponse(Call<QLResponse> call, Response<QLResponse> response) {
                    QLResponse qlResponse = response.body();
                    if (qlResponse != null) {
                        responseView.setText("RESPONSE: " + qlResponse.toString());
                    } else {
                        Toast.makeText(getApplicationContext(), "Invalid query response", Toast.LENGTH_SHORT)
                            .show();
                    }
                }

                @Override
                public void onFailure(Call<QLResponse> call, Throwable t) {
                    t.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Failed query", Toast.LENGTH_SHORT)
                        .show();
                }
            });
    }
}
